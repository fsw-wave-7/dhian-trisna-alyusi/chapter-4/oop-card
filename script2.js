
class Card {
    constructor(cardId) {
        this.cardId = cardId
        this._chosen = null
        this.options = [
            {
                name: 'dog',
                url: 'https://image.freepik.com/free-vector/cute-dog-sticking-her-tongue-out-cartoon-icon-illustration_138676-2709.jpg'
            },
            {
                name: 'bear',
                url: 'https://image.freepik.com/free-vector/cute-teddy-bear-waving-hand-cartoon-icon-illustration_138676-2714.jpg'
            },
            {
                name: 'cat',
                url: 'https://image.freepik.com/free-vector/cute-cat-with-cat-bowl-cartoon-icon-illustration_138676-2707.jpg'
            }
        ]
    }

    get chosen() {
        return this._chosen
    }

    set chosen(card) {
        this._chosen = card
    }

    getCard() {
        document.getElementById(this.cardId).innerHTML = ''

        const t = this
        return this.options.forEach((opt) => {
            const img = document.createElement('img')
            img.src = opt.url
            img.className = 'img'
            img.id = `${this.cardId}-${opt.name}`

            document.getElementById(this.cardId).append(img)
        })
    }

    getOptions() {
        return this.options;
    }

    setActive(id) {
        document.getElementById(id).classList.add('active')
    }

    resetCard() {
        options.forEach((opt) => {
            document.getElementById(`${this.cardId}-${opt.name}`).classList.remove('active')
        })
    }
}

class RandomCard extends Card {
    constructor(cardId) {
        super(cardId)
    }

    random() {
        let index = Math.floor(Math.random() * Math.floor(3))
        const options = super.getOptions()
        return options[index]
    }
}

const card = new Card('cards')
card.getCard()
const cardComp = new RandomCard('cards-comp')
cardComp.getCard()

function getResult(player, comp) {
    let text = ''
    if (player.name == comp.name) {
        text = `Player and Comp choose same card : ${player.name}`
    } else {
        text = `Player and Comp choose different card. Player : ${player.name} AND Comp : ${comp.name}`
    }

    document.getElementsByClassName('result')[0].innerHTML = text
    card.setActive(`cards-${card.chosen.name}`)
    cardComp.setActive(`cards-comp-${cardComp.chosen.name}`)
}

const options = card.getOptions()
options.forEach((opt) => {
    document.getElementById(`cards-${opt.name}`).addEventListener('click', () => {
        card.chosen = opt
        cardComp.chosen = cardComp.random()
        card.resetCard()
        cardComp.resetCard()
        getResult(card.chosen, cardComp.chosen)
    })
})

document.getElementById('reset').addEventListener('click', () => {
    card.chosen = null
    cardComp.chosen = null
    card.resetCard()
    cardComp.resetCard()
    document.getElementsByClassName('result')[0].innerHTML = ''
})
